from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import daftar, savedata, donasi, savedatadonasi
from .models import Model_Daftar, Model_Donasi
from .forms import Form_Daftar, Form_Donasi
# Create your tests here.

class SinarPerakUnitTest(TestCase):
	
	def test_daftar_url_is_exist(self):
		response = Client().get('/crowd/daftar')
		self.assertEqual(response.status_code, 200)

	def test_daftar_using_daftar_func(self):
		found = resolve('/crowd/daftar')
		self.assertEqual(found.func, daftar)

	def test_daftar_using_formdaftar_template(self):
		response = Client().get('/crowd/daftar')
		self.assertTemplateUsed(response, 'formdaftar.html')

	def test_donasi_url_is_exist(self):
		response = Client().get('/crowd/donasi')
		self.assertEqual(response.status_code, 200)

	def test_donasi_using_donasi_func(self):
		found = resolve('/crowd/donasi')
		self.assertEqual(found.func, donasi)

	def test_donasi_using_formdonasi_template(self):
		response = Client().get('/crowd/donasi')
		self.assertTemplateUsed(response, 'formdonasi.html')