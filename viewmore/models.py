from django.db import models

# Create your models here.
class Program(models.Model):
	title = models.CharField(max_length=20)
	text_test = models.CharField(max_length=50)
	terkumpul = models.IntegerField()

class Berita(models.Model):
	title = models.CharField(max_length=20)
	paragraph = models.CharField(max_length=1000)

class Status(models.Model):
	Kabar = models.CharField(max_length = 10000)
	Waktu = models.DateTimeField(auto_now_add = True)


