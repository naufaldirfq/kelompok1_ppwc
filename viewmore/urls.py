from django.urls import path
from .views import *
urlpatterns = [
    path('', index, name='index'),
    path('berita', viewnews, name= 'news'),
    path('program', viewprogram, name= 'program'),
    path('program/<int:id>/', dynamic, name= 'program'),
    path('comment',story6_status, name= 'comment'),
    path('status/add_activity', story6_status_add, name="status_add"),
    path('getuser',getUser, name= 'getUser'),
]