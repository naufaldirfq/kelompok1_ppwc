from django.urls import path
from history import views

urlpatterns = [
    path('', views.history_func,  name="history"),
]
