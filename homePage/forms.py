from django import forms
from .models import Jadwal
from django.forms import ModelForm

class Message_Form(ModelForm):
	class Meta:
		model = Jadwal
		fields = ['Kegiatan','tempat','kategori','tanggal','waktu']